import { Component, OnInit } from '@angular/core';

import { theQuestionDTO } from './question.model';
import { QuestionService } from './question.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})

export class QuestionComponent implements OnInit {

questions:any[]=[];

  constructor(private questionService:QuestionService) { }

  ngOnInit(): void {
    this.getQeustions();
  }


  getQeustions():void{
    this.questionService.getQuestions().subscribe((q) =>{
      this.questions = q.questionList;
      console.log(q);
    });
    }

    deleteQeustion(id:number):void{
      this.questionService.deleteQuestion(id).subscribe(()=>{
        console.log(`Item # ${id} is deleted!`);

        this.getQeustions();
        this.getQeustions(); this.getQeustions(); this.getQeustions();

      });
    }
    deleteQuestions():void{
      this.questionService.deleteQeustions().subscribe(()=>{
        console.log(`All answers were deleted!!`);
        this.getQeustions();
        this.getQeustions();
      });
    }
}
